﻿using encuesta.webapi.Areas.Interface;
using encuesta.webapi.Configurations;
using encuesta.webapi.DTO.Usuario;
using Microsoft.AspNetCore.Mvc;

namespace encuesta.webapi.Areas.Controllers
{
    [Route("api/[controller]")]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioApplication usuarioApplication;

        public UsuarioController(IUsuarioApplication usuarioApplication)
        {
            this.usuarioApplication = usuarioApplication;
        }

        /// <summary>
        /// Logeo de usuario
        /// </summary>
        [HttpGet]
        [Route("login")]
        public async Task<OperationResult<LoginRespuestaDto>> Login(string nomUsuario, string claveUsuario)
        {
            var resultado = await usuarioApplication.Login(nomUsuario, claveUsuario);
            return resultado;
        }
    }
}
