﻿using encuesta.webapi.Areas.Interface;
using encuesta.webapi.Configurations;
using encuesta.webapi.DTO.Encuesta;
using Microsoft.AspNetCore.Mvc;

namespace encuesta.webapi.Areas.Controllers
{
    [Route("api/[controller]")]
    public class EncuestaController : Controller
    {
        private readonly IEncuestaApplication encuestaApplication;

        public EncuestaController(IEncuestaApplication encuestaApplication)
        {
            this.encuestaApplication = encuestaApplication;
        }

        /// <summary>
        /// Obtener la encuesta
        /// </summary>
        [HttpGet]
        [Route("obtenerEncuesta")]
        public async Task<OperationResult<ObtenerEncuestaRespuestaDto>> ObtenerEncuesta(int codUsuario)
        {
            var resultado = await encuestaApplication.ObtenerEncuesta(codUsuario);
            return resultado;
        }
        /// <summary>
        /// Obtener reporte de la encuesta
        /// </summary>
        [HttpGet]
        [Route("obtenerReporte")]
        public async Task<OperationResult<ObtenerReporteRespuestaDto>> ObtenerReporte(int codEncuesta)
        {
            var resultado = await encuestaApplication.ObtenerReporte(codEncuesta);
            return resultado;
        }
        /// <summary>
        /// Registrar votacion
        /// </summary>
        [HttpPost]
        [Route("registrarVotacion")]
        public async Task<OperationResult<string>> RegistrarVotacion([FromBody] RegistrarVotacionPeticionDto registrarVotacionPeticionDto)
        {
            var resultado = await encuestaApplication.RegistrarVotacion(registrarVotacionPeticionDto);
            return resultado;
        }
    }
}
