﻿using encuesta.webapi.Configurations;
using encuesta.webapi.DTO.Usuario;

namespace encuesta.webapi.Areas.Interface
{
    public interface IUsuarioApplication
    {
        Task<OperationResult<LoginRespuestaDto>> Login(string nomUsuario, string claveUsuario);
    }
}
