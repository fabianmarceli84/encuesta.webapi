﻿using encuesta.webapi.Configurations;
using encuesta.webapi.DTO.Encuesta;

namespace encuesta.webapi.Areas.Interface
{
    public interface IEncuestaApplication
    {
        Task<OperationResult<ObtenerEncuestaRespuestaDto>> ObtenerEncuesta(int codUsuario);
        Task<OperationResult<ObtenerReporteRespuestaDto>> ObtenerReporte(int codEncuesta);
        Task<OperationResult<string>> RegistrarVotacion(RegistrarVotacionPeticionDto registrarVotacionPeticionDto);
    }
}
