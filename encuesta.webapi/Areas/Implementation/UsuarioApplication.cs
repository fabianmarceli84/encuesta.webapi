﻿using encuesta.webapi.Areas.Interface;
using encuesta.webapi.Configurations;
using encuesta.webapi.DTO.Usuario;
using System.Data;

namespace encuesta.webapi.Areas.Implementation
{
    public class UsuarioApplication : IUsuarioApplication
    {
        public async Task<OperationResult<LoginRespuestaDto>> Login(string nomUsuario, string claveUsuario)
        {
            var resultado = new OperationResult<LoginRespuestaDto> { isValid = false, exceptions = new List<OperationException>() };
            try
            {
                var parametros = $"{nomUsuario}|{claveUsuario}";

                Console.WriteLine(DateTime.Now + ": dbo.ProcUsuario|10|Encuesta " + parametros);

                var ds = await new ProcedureGeneral().Procedure(new ProcedureRequestDto()
                {
                    Procedimiento = "dbo.ProcUsuario",
                    Parametro = parametros,
                    Indice = 10,
                    Database = "Encuesta"
                });

                var usuario = (from x in ds.Tables[0].AsEnumerable() select x).FirstOrDefault();

                if (usuario?.Field<int?>("CodResultado") == 0)
                {
                    resultado.exceptions.Add(new OperationException("E001", usuario?.Field<string>("DesResultado") ?? ""));
                    Console.WriteLine(DateTime.Now + ": " + resultado);
                    return resultado;
                }

                var response = new LoginRespuestaDto
                {
                    CodUsuario = usuario?.Field<int?>("CodUsuario") ?? 0,
                    CodUsuarioTipo = usuario?.Field<int?>("CodUsuarioTipo") ?? 0
                };

                resultado.isValid = true;
                resultado.content = response;

                Console.WriteLine(DateTime.Now + ": " + resultado);

                return resultado;
            }
            catch (Exception e)
            {
                Console.WriteLine(DateTime.Now + ": " + e);
                throw new Exception(e.Message);
            }
        }
    }
}
