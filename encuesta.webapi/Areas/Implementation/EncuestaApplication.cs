﻿using encuesta.webapi.Areas.Interface;
using encuesta.webapi.Configurations;
using encuesta.webapi.DTO.Encuesta;
using System.Data;

namespace encuesta.webapi.Areas.Implementation
{
    public class EncuestaApplication : IEncuestaApplication
    {
        public async Task<OperationResult<ObtenerEncuestaRespuestaDto>> ObtenerEncuesta(int codUsuario)
        {
            var resultado = new OperationResult<ObtenerEncuestaRespuestaDto> { isValid = false, exceptions = new List<OperationException>() };
            try
            {
                var parametros = $"{codUsuario}";

                Console.WriteLine(DateTime.Now + ": dbo.ProcEncuesta|10|Encuesta " + parametros);

                var ds = await new ProcedureGeneral().Procedure(new ProcedureRequestDto()
                {
                    Procedimiento = "dbo.ProcEncuesta",
                    Parametro = parametros,
                    Indice = 10,
                    Database = "Encuesta"
                });

                var encuesta = (from x in ds.Tables[0].AsEnumerable() select x).FirstOrDefault();

                var response = new ObtenerEncuestaRespuestaDto
                {
                    CodEncuestaDetalle = encuesta?.Field<int?>("CodEncuestaDetalle") ?? 0,
                    Pregunta = encuesta?.Field<string?>("Pregunta") ?? "",
                    RespondioCliente = encuesta?.Field<int?>("RespondioCliente") ?? 0
                };

                resultado.isValid = true;
                resultado.content = response;

                Console.WriteLine(DateTime.Now + ": " + resultado);

                return resultado;
            }
            catch (Exception e)
            {
                Console.WriteLine(DateTime.Now + ": " + e);
                throw new Exception(e.Message);
            }
        }
        public async Task<OperationResult<ObtenerReporteRespuestaDto>> ObtenerReporte(int codEncuesta)
        {
            var resultado = new OperationResult<ObtenerReporteRespuestaDto> { isValid = false, exceptions = new List<OperationException>() };
            try
            {
                var parametros = $"{codEncuesta}";

                Console.WriteLine(DateTime.Now + ": dbo.ProcEncuesta|11|Encuesta " + parametros);

                var ds = await new ProcedureGeneral().Procedure(new ProcedureRequestDto()
                {
                    Procedimiento = "dbo.ProcEncuesta",
                    Parametro = parametros,
                    Indice = 11,
                    Database = "Encuesta"
                });

                var reporte = (from x in ds.Tables[0].AsEnumerable() select x).FirstOrDefault();

                var response = new ObtenerReporteRespuestaDto
                {
                    CodEncuestaDetalle = reporte?.Field<int?>("CodEncuestaDetalle") ?? 0,
                    NomEncuesta = reporte?.Field<string?>("NomEncuesta") ?? "",
                    Pregunta = reporte?.Field<string?>("Pregunta") ?? "",
                    NumRespuestaEncuesta = reporte?.Field<int?>("NumRespuestaEncuesta") ?? 0,
                    CantidadDetractores = reporte?.Field<int?>("CantidadDetractores") ?? 0,
                    CantidadNeutros = reporte?.Field<int?>("CantidadNeutros") ?? 0,
                    CantidadPromotores = reporte?.Field<int?>("CantidadPromotores") ?? 0,
                    NPS = reporte?.Field<decimal?>("NPS") ?? 0
                };

                resultado.isValid = true;
                resultado.content = response;

                Console.WriteLine(DateTime.Now + ": " + resultado);

                return resultado;
            }
            catch (Exception e)
            {
                Console.WriteLine(DateTime.Now + ": " + e);
                throw new Exception(e.Message);
            }
        }
        public async Task<OperationResult<string>> RegistrarVotacion(RegistrarVotacionPeticionDto registrarVotacionPeticionDto)
        {
            var resultado = new OperationResult<string>() { isValid = false, exceptions = new List<OperationException>() };
            try
            {
                var rvp = registrarVotacionPeticionDto;
                var parametros = $"{rvp.CodUsuario}|{rvp.CodEncuestaDetalle}|{rvp.RespuestaPregunta}";

                Console.WriteLine(DateTime.Now + ": dbo.ProcEncuesta|20|Cliente " + parametros);

                var ds = await new ProcedureGeneral().Procedure(new ProcedureRequestDto()
                {
                    Procedimiento = "dbo.ProcEncuesta",
                    Parametro = parametros,
                    Indice = 20,
                    Database = "Encuesta"
                });

                var respBd = (from x in ds.Tables[0].AsEnumerable() select x).FirstOrDefault();

                if (respBd?.Field<int?>("CodResultado") == 0)
                {
                    resultado.exceptions.Add(new OperationException("E001", respBd?.Field<string>("DesResultado") ?? ""));
                    Console.WriteLine(DateTime.Now + ": " + resultado);
                    return resultado;
                }

                resultado.isValid = true;
                resultado.content = respBd?.Field<string?>("DesResultado") ?? "";

                Console.WriteLine(DateTime.Now + ": " + resultado);

                return resultado;
            }
            catch (Exception e)
            {
                Console.WriteLine(DateTime.Now + ": " + e);
                throw new Exception(e.Message);
            }
        }
    }
}
