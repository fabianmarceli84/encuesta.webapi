﻿namespace encuesta.webapi.DTO.Encuesta
{
    public class ObtenerReporteRespuestaDto
    {
        public int CodEncuestaDetalle { get; set; }
        public string? NomEncuesta { get; set; }
        public string? Pregunta { get; set; }
        public int NumRespuestaEncuesta { get; set; }
        public int CantidadDetractores { get; set; }
        public int CantidadNeutros { get; set; }
        public int CantidadPromotores { get; set; }
        public decimal NPS { get; set; }
    }
}
