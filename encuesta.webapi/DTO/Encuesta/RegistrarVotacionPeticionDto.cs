﻿namespace encuesta.webapi.DTO.Encuesta
{
    public class RegistrarVotacionPeticionDto
    {
        public int CodUsuario { get; set; }
        public int CodEncuestaDetalle { get; set; }
        public string? RespuestaPregunta { get; set; }
    }
}
