﻿namespace encuesta.webapi.DTO.Encuesta
{
    public class ObtenerEncuestaRespuestaDto
    {
        public int CodEncuestaDetalle { get; set; }
        public string? Pregunta { get; set; }
        public int RespondioCliente { get; set; }
    }
}
