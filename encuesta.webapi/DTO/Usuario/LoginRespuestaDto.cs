﻿namespace encuesta.webapi.DTO.Usuario
{
    public class LoginRespuestaDto
    {
        public int CodUsuario { get; set; }
        public int CodUsuarioTipo { get; set; }
    }
}
