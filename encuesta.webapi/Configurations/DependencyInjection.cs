﻿using encuesta.webapi.Areas.Implementation;
using encuesta.webapi.Areas.Interface;

namespace encuesta.webapi.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static void Config(IServiceCollection services)
        {
            services.AddTransient<IUsuarioApplication, UsuarioApplication>();
            services.AddTransient<IEncuestaApplication, EncuestaApplication>();
        }
    }
}
